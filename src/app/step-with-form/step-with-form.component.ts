import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-step-with-form',
  templateUrl: './step-with-form.component.html',
  styleUrls: [ './step-with-form.component.scss' ]
})
export class StepWithFormComponent implements OnInit {

  @Output() valid = new EventEmitter();
  checkbox: FormControl;

  constructor() {
  }

  ngOnInit(): void {
    this.checkbox = new FormControl(false, [Validators.required]);

    this.checkbox.valueChanges
      .subscribe(
        (status) => this.valid.emit(status)
      );
  }

}

