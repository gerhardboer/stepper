import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepWithFormComponent } from './step-with-form.component';

describe('StepWithFormComponent', () => {
  let component: StepWithFormComponent;
  let fixture: ComponentFixture<StepWithFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepWithFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepWithFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
