import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StepperModule } from './stepper/stepper.module';
import { MatButtonModule } from '@angular/material/button';
import { StepWithFormComponent } from './step-with-form/step-with-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    AppComponent,
    StepWithFormComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    StepperModule,
    MatButtonModule,
    ReactiveFormsModule, MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
