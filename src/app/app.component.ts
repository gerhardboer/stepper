import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'stepper';
  private valid: boolean;

  validateFn = () => this.valid;

  validated(valid) {
    this.valid = valid;
  }
}
