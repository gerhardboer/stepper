import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepflowComponent } from './stepflow/stepflow.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { StepFlowStepDirective } from './step-flow-step.directive';

@NgModule({
  declarations: [StepflowComponent, StepFlowStepDirective],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule
  ],
  exports: [
    StepflowComponent, StepFlowStepDirective
  ]
})
export class StepperModule { }
