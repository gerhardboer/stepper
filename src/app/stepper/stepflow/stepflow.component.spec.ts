import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepflowComponent } from './stepflow.component';

describe('StepflowComponent', () => {
  let component: StepflowComponent;
  let fixture: ComponentFixture<StepflowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepflowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepflowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
