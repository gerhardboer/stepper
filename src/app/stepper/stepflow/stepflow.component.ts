import { AfterContentInit, Component, ContentChildren, OnInit, QueryList, TemplateRef } from '@angular/core';
import { StepFlowStepDirective } from '../step-flow-step.directive';

@Component({
  selector: 'app-stepflow',
  templateUrl: './stepflow.component.html',
  styleUrls: [ './stepflow.component.scss' ]
})
export class StepflowComponent implements OnInit, AfterContentInit {

  @ContentChildren(StepFlowStepDirective, { read: TemplateRef }) stepTemplates: QueryList<TemplateRef<StepFlowStepDirective>>;
  @ContentChildren(StepFlowStepDirective) stepsDirectives: QueryList<StepFlowStepDirective>;

  activeStep: TemplateRef<StepFlowStepDirective>;
  activeStepDirective: StepFlowStepDirective;
  previousStep: TemplateRef<StepFlowStepDirective>;

  private steps: TemplateRef<StepFlowStepDirective>[];
  private stepDirs: StepFlowStepDirective[];

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterContentInit() {
    this.steps = this.stepTemplates.toArray();
    this.stepDirs = this.stepsDirectives.toArray();
    this.activeStep = this.steps[ 0 ];
    this.activeStepDirective = this.stepDirs[ 0 ];
  }

  next() {
    if (this.activeStepDirective.nextAllowed()) {
      const current = this.steps.indexOf(this.activeStep);
      if (current < (this.steps.length - 1)) {
        this.previousStep = this.steps[ current ];
        this.activeStep = this.steps[ current + 1 ];
        this.activeStepDirective = this.stepDirs[ current + 1 ];
      }
    }
  }

  previous() {
    const current = this.steps.indexOf(this.activeStep);
    if (current > 0) {
      this.activeStep = this.steps[ current - 1 ];
      this.activeStepDirective = this.stepDirs[ current - 1 ];
    }

    if (current > 1) {
      this.previousStep = this.steps[ current - 2 ];
    } else {
      this.previousStep = null;
    }
  }

  get context() {
    return this;
  }
}
