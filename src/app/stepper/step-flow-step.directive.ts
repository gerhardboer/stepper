import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[appStep]'
})
export class StepFlowStepDirective {

  @Input() next = 'Next';
  @Input() previous = 'Previous';
  @Input() nextAllowed = () => true;

  constructor() { }
}
